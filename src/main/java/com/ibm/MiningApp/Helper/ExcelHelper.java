package com.ibm.MiningApp.Helper;

import com.google.gson.Gson;
import com.ibm.MiningApp.Dao.RowHeader;
import com.ibm.MiningApp.Dao.Tab;
import com.ibm.MiningApp.dto.AuditingAreasSectionDto;
import com.ibm.MiningApp.dto.QuestionaireDto;
import com.ibm.MiningApp.dto.TabsDto;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

public class ExcelHelper {
    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    public static boolean hasExcelFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }


    public static String getSectionId(String wbString) {
        String Id = "";
        String[] stringTest = wbString.split(" ");
        for (int j = 0; j <= stringTest.length - 1; j++) {
            Id = Id.concat(stringTest[j].replaceFirst(" ", "").substring(0, 1)).toUpperCase();
        }
        return Id;
    }

    public static void readAuditingAreaSections(Iterator<Row> itr, String areaName, XSSFWorkbook wb,List<AuditingAreasSectionDto> auditingAreasSectionDtoList,List<QuestionaireDto> questionaireDtoList) {


        List<RowHeader> rowHeaders = new ArrayList<>();
        List<String> questions = new ArrayList<>();

        String sectionText = "";
        int count = 0;
        while (itr.hasNext()) {

            Row row = itr.next();
            RowHeader rowHeader = new RowHeader();

            if (row.getRowNum() >= 0) {
                Iterator<Cell> columnIterator = row.cellIterator();

                while (columnIterator.hasNext()) {

                    Cell cellValue = columnIterator.next();
                    int fontIndex = cellValue.getCellStyle().getFontIndex();

                    Font font = wb.getFontAt(fontIndex);

                    if (font.getBold()) {
                        rowHeader.setHeaderName(cellValue.getStringCellValue());
sectionText = cellValue.getStringCellValue();
                        auditingAreasSectionDtoList.add(getAuditingAreaSection(areaName,cellValue,sectionText));

                    } else {
                        questions.add(cellValue.getStringCellValue());
                        questionaireDtoList.add(getQuestionnaire(areaName,sectionText,count,cellValue));

                    }

                    rowHeader.setQuestionnaire(questions);
                }
                rowHeaders.add(rowHeader);

            }
break;

        }


    }

    private static AuditingAreasSectionDto getAuditingAreaSection(String areaName, Cell cellValue, String sectionText) {

        AuditingAreasSectionDto auditingAreasSectionDto = new AuditingAreasSectionDto();


        auditingAreasSectionDto.setSectionName(cellValue.getStringCellValue().replace("&", "and"));
        auditingAreasSectionDto.setAuditingAreaName(areaName);
        auditingAreasSectionDto.setId(getSectionId(areaName) + "-" + getSectionId(cellValue.getStringCellValue().replace("&", "and")));

        return auditingAreasSectionDto;
    }

    private static QuestionaireDto getQuestionnaire(String areaName, String sectionText, int count, Cell cellValue) {
        QuestionaireDto questionaireDto = new QuestionaireDto();
        questionaireDto.setSectionId(new StringBuilder().append(getSectionId(areaName)).append("-").append(sectionText).toString());
        questionaireDto.setQuestion(cellValue.getStringCellValue());
        questionaireDto.setId(new StringBuilder().append(getSectionId(areaName)).append("-").append(sectionText).toString() + "-" + count);

        count++;
        return questionaireDto;
    }
}