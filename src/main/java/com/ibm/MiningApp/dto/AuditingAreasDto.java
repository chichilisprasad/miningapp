package com.ibm.MiningApp.dto;

import java.util.List;
import java.util.Objects;

public class AuditingAreasDto {
    private String Id;

    private String AuditingArea;

    private List<AuditingAreasSectionDto> auditingAreasSectionDtoList;
    public String getId() {
        return Id;
    }

    @Override
    public String toString() {
        return "AuditingAreasDto{" +
                "Id='" + Id + '\'' +
                ", AuditingArea='" + AuditingArea + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuditingAreasDto that = (AuditingAreasDto) o;
        return Objects.equals(Id, that.Id) && Objects.equals(AuditingArea, that.AuditingArea);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, AuditingArea);
    }

    public void setId(String id) {
        Id = id;
    }

    public String getAuditingArea() {
        return AuditingArea;
    }

    public void setAuditingArea(String auditingArea) {
        AuditingArea = auditingArea;
    }

}
