package com.ibm.MiningApp.dto;

import java.util.List;
import java.util.Objects;

public class AuditingAreasSectionDto {
    private String Id;
    private String SectionName;
    private String auditingAreaName;

    public String getAuditingAreaName() {
        return auditingAreaName;
    }

    public void setAuditingAreaName(String auditingAreaName) {
        this.auditingAreaName = auditingAreaName;
    }

    @Override
    public String toString() {
        return "AuditingAreasSectionDto{" +
                "Id='" + Id + '\'' +
                ", SectionName='" + SectionName + '\'' +
                ", auditingAreaName='" + auditingAreaName + '\'' +
                ", questionnaire=" + questionnaire +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuditingAreasSectionDto that = (AuditingAreasSectionDto) o;
        return Objects.equals(Id, that.Id) && Objects.equals(SectionName, that.SectionName) && Objects.equals(auditingAreaName, that.auditingAreaName) && Objects.equals(questionnaire, that.questionnaire);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, SectionName, auditingAreaName, questionnaire);
    }

    private List<String> questionnaire;


    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getSectionName() {
        return SectionName;
    }

    public void setSectionName(String sectionName) {
        SectionName = sectionName;
    }

    public List<String> getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(List<String> questionnaire) {
        this.questionnaire = questionnaire;
    }

}
