package com.ibm.MiningApp.dto;

import java.util.List;
import java.util.Objects;

public class TabsDto {
    List<AuditingAreasDto> auditingAreasDtoList;
    List<AuditingAreasSectionDto> auditingAreasSectionDtoList;
    List<QuestionaireDto> questionaireDtoList;

    public List<AuditingAreasDto> getAuditingAreasDtoList() {
        return auditingAreasDtoList;
    }

    public void setAuditingAreasDtoList(List<AuditingAreasDto> auditingAreasDtoList) {
        this.auditingAreasDtoList = auditingAreasDtoList;
    }

    public List<AuditingAreasSectionDto> getAuditingAreasSectionDtoList() {
        return auditingAreasSectionDtoList;
    }

    public void setAuditingAreasSectionDtoList(List<AuditingAreasSectionDto> auditingAreasSectionDtoList) {
        this.auditingAreasSectionDtoList = auditingAreasSectionDtoList;
    }

    public List<QuestionaireDto> getQuestionaireDtoList() {
        return questionaireDtoList;
    }

    public void setQuestionaireDtoList(List<QuestionaireDto> questionaireDtoList) {
        this.questionaireDtoList = questionaireDtoList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TabsDto tabsDto = (TabsDto) o;
        return Objects.equals(auditingAreasDtoList, tabsDto.auditingAreasDtoList) && Objects.equals(auditingAreasSectionDtoList, tabsDto.auditingAreasSectionDtoList) && Objects.equals(questionaireDtoList, tabsDto.questionaireDtoList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(auditingAreasDtoList, auditingAreasSectionDtoList, questionaireDtoList);
    }

    @Override
    public String toString() {
        return "TabsDto{" +
                "auditingAreasDtoList=" + auditingAreasDtoList +
                ", auditingAreasSectionDtoList=" + auditingAreasSectionDtoList +
                ", questionaireDtoList=" + questionaireDtoList +
                '}';
    }
}
