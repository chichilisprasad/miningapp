package com.ibm.MiningApp.dto;

import java.util.Objects;

public class QuestionaireDto {
    private String Id;
    private String SectionId;
    private String Question;
    private boolean isDeleted = false;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getSectionId() {
        return SectionId;
    }

    public void setSectionId(String sectionId) {
        SectionId = sectionId;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestionaireDto that = (QuestionaireDto) o;
        return isDeleted == that.isDeleted && Objects.equals(Id, that.Id) && Objects.equals(SectionId, that.SectionId) && Objects.equals(Question, that.Question);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, SectionId, Question, isDeleted);
    }

    @Override
    public String toString() {
        return "QuestionaireDto{" +
                "Id='" + Id + '\'' +
                ", SectionId='" + SectionId + '\'' +
                ", Question='" + Question + '\'' +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
