package com.ibm.MiningApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiningAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiningAppApplication.class, args);
	}


}
