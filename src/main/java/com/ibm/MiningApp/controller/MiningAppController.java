package com.ibm.MiningApp.controller;

import com.google.gson.Gson;
import com.ibm.MiningApp.Dao.ExcelDAOImpl;
import com.ibm.MiningApp.Dao.Tab;
import com.ibm.MiningApp.Helper.ExcelHelper;
import com.ibm.MiningApp.dto.AuditingAreasDto;
import com.ibm.MiningApp.dto.AuditingAreasSectionDto;
import com.ibm.MiningApp.dto.QuestionaireDto;

import com.ibm.MiningApp.dto.TabsDto;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class MiningAppController {
    private static final String idVal = "ADA";
    @RequestMapping(method = RequestMethod.POST, path = "/api/excel/upload")
    public void uploadExcelData(@RequestParam("file") MultipartFile file) {
        List<QuestionaireDto> questionaireDtoList = new ArrayList<>();
        List<AuditingAreasSectionDto> auditingAreasSectionDtoList = new ArrayList<>();
List<Tab> tabList = new ArrayList<>();


        XSSFWorkbook wb;
        if (ExcelHelper.hasExcelFormat(file)) {
            try {
                InputStream fis = file.getInputStream(); // obtaining bytes from the file
                // creating Workbook instance that refers to .xlsx file
                wb = new XSSFWorkbook(fis);
              final AtomicInteger count = new AtomicInteger(0);

                for (int i = 0; i < wb.getNumberOfSheets(); i++) {

                    XSSFSheet sheet = wb.getSheetAt(i);
                    Tab tab = new Tab();
                    tab.setTabName(wb.getSheetName(i));
                    tab.setId(idVal + count.incrementAndGet());
                    tab.setVersionId(1);
                  tabList.add(tab);
                  ExcelHelper.readAuditingAreaSections(sheet.iterator(), wb.getSheetName(i), wb,auditingAreasSectionDtoList,questionaireDtoList);


                }

                ExcelDAOImpl.insertExcelMasterata(tabList);
                ExcelDAOImpl.insertExcelSectionsData(auditingAreasSectionDtoList);
                ExcelDAOImpl.insertExcelQuestionsData(questionaireDtoList);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private String getId(XSSFWorkbook wb, int i) {
        String Id = "";
        String[] stringTest = wb.getSheetName(i).split(" ");
        for (int j = 0; j < stringTest.length; j++) {
            Id = Id.concat(stringTest[j].substring(0, 1));
        }
        return Id;
    }
}
