package com.ibm.MiningApp.Dao;

import java.util.List;
import java.util.Objects;

public class Tab {
    List<RowHeader> rowHeaders;
    String tabName;
    String id;
    int VersionId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tab tab = (Tab) o;
        return VersionId == tab.VersionId && Objects.equals(rowHeaders, tab.rowHeaders) && Objects.equals(tabName, tab.tabName) && Objects.equals(id, tab.id);
    }

    @Override
    public String toString() {
        return "Tab{" +
                "rowHeaders=" + rowHeaders +
                ", tabName='" + tabName + '\'' +
                ", id='" + id + '\'' +
                ", VersionId=" + VersionId +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(rowHeaders, tabName, id, VersionId);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVersionId() {
        return VersionId;
    }

    public void setVersionId(int versionId) {
        VersionId = versionId;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public List<RowHeader> getRowHeaders() {
        return rowHeaders;
    }

    public void setRowHeaders(List<RowHeader> rowHeaders) {
        this.rowHeaders = rowHeaders;
    }
}
