package com.ibm.MiningApp.Dao;



import com.ibm.MiningApp.dto.AuditingAreasSectionDto;
import com.ibm.MiningApp.dto.QuestionaireDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ExcelDAOImpl {


    private static final String  MASTER_DATA_INSERT= "INSERT INTO master_data1(tabName,sectionName,questionName) " +
            "VALUES (?,?,?)";

    private static final String  MASTER_TAB_DATA_INSERT= "INSERT INTO Master_Auditing_Areas1(id,tabName,versionId) " +
            "VALUES (?,?,?)";

    private static final String  MASTER_TAB_SECTIONS_DATA_INSERT= "INSERT INTO Master_Auditing_Area_Section1(id,sectionName,areaName) " +
            "VALUES (?,?,?)";

    private static final String  MASTER_TAB_SECTIONS_QUESTIONS_DATA_INSERT= "INSERT INTO Master_Auditing_Area_Section_Questions1(id,sectionId,questionName) " +
            "VALUES (?,?,?)";

    public static void insertExcelMasterata(List<Tab> tabList) {

        DBUtil dbUtil = new DBUtil();
        Connection connection = dbUtil.getConnection();

        try {
            try (PreparedStatement statement = connection.prepareStatement(MASTER_TAB_DATA_INSERT)) {
                tabList.forEach(tab -> {
//                    try {
//                        tab.getRowHeaders().forEach(x -> {
//                            x.getQuestionnaire().forEach(y -> {
                                try {
                                    statement.setString(1, tab.getTabName());
                                    statement.setString(2,tab.getId());
                                    statement.setInt(3, tab.getVersionId());
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }

                                try {
                                    statement.addBatch();
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
//                            });
//                        });

//                    } catch (Exception ignored) {
//                    }
                });
                statement.executeBatch();
//                insertExcelMasterTabs();
            }
        } catch (Exception e) {
            Logger.getLogger("error {}",e.getMessage() );
              } finally {
            dbUtil.closeConnection(null);
        }
    }

    public static void insertExcelSectionsData(List<AuditingAreasSectionDto> auditingAreasSectionDtoList) {

        DBUtil dbUtil = new DBUtil();
        Connection connection = dbUtil.getConnection();

        try {
            try (PreparedStatement statement = connection.prepareStatement(MASTER_TAB_SECTIONS_DATA_INSERT)) {
                auditingAreasSectionDtoList.forEach(tab -> {
//
                    try {
                        statement.setString(1, tab.getId());
                        statement.setString(2,tab.getSectionName());
                        statement.setString(3, tab.getAuditingAreaName());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    try {
                        statement.addBatch();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                });
                statement.executeBatch();

            }
        } catch (Exception e) {
            Logger.getLogger("error {}",e.getMessage() );
        } finally {
            dbUtil.closeConnection(null);
        }
    }

    public static void insertExcelQuestionsData(List<QuestionaireDto> questionaireDtoList) {

        DBUtil dbUtil = new DBUtil();
        Connection connection = dbUtil.getConnection();

        try {
            try (PreparedStatement statement = connection.prepareStatement(MASTER_TAB_SECTIONS_QUESTIONS_DATA_INSERT)) {
                questionaireDtoList.forEach(tab -> {
//
                    try {
                        statement.setString(1, tab.getId());
                        statement.setString(2,tab.getSectionId());
                        statement.setString(3, tab.getQuestion());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    try {
                        statement.addBatch();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                });
                statement.executeBatch();

            }
        } catch (Exception e) {
            Logger.getLogger("error {}",e.getMessage() );
        } finally {
            dbUtil.closeConnection(null);
        }
    }
    public static List<String> getTabsDataFromDB(){

        DBUtil dbUtil = null;
        ResultSet rss = null;
        List<String> tabs = new ArrayList<>();

        final String tabQry = "select tabName from master_data";

        try{
            dbUtil = new DBUtil();
            rss  = dbUtil.executePreparedStatment(tabQry);
            while(rss.next()){
                assert false;
                tabs.add(rss.getString("tabName"));
            }
        }catch(Exception e){
            Logger.getLogger("error {}",e.getMessage() );
        }finally{

            assert dbUtil != null;
            dbUtil.closeConnection(rss);
        }

        return tabs;
    }

    public static List<String> getSectionNamesWithTabNameFromDB(String tabName){

        DBUtil dbUtil = null;
        ResultSet rss = null;
        List<String> sections = new ArrayList<>();

        final String tabQry = "select sectionName from master_data where tabName='"+tabName+"'";

        try{
            dbUtil = new DBUtil();
            rss  = dbUtil.executePreparedStatment(tabQry);
            while(rss.next()){
                assert false;
                sections.add(rss.getString("sectionName"));
            }
        }catch(Exception e){
            Logger.getLogger("error {}",e.getMessage() );
        }finally{

            assert dbUtil != null;
            dbUtil.closeConnection(rss);
        }

        return sections;
    }

    public static List<String> getQuestionNamesWithSectionNameFromDB(String sectionName){

        DBUtil dbUtil = null;
        ResultSet rss = null;
        List<String> sections = new ArrayList<>();

        final String sectionQry = "select questionName from master_data where sectionName='"+sectionName+"'";

        try{
            dbUtil = new DBUtil();
            rss  = dbUtil.executePreparedStatment(sectionQry);
            while(rss.next()){
                assert false;
                sections.add(rss.getString("questionName"));
            }
        }catch(Exception e){
            Logger.getLogger("error {}",e.getMessage() );
        }finally{

            assert dbUtil != null;
            dbUtil.closeConnection(rss);
        }

        return sections;
    }

    public static void insertExcelMasterTabs() {

        DBUtil dbUtil = new DBUtil();
        Connection connection = dbUtil.getConnection();
        try {
            List<String> tabs = getTabsDataFromDB()
                    .stream()
                    .distinct()
                    .collect(Collectors.toList());


            Connection sectionConnection = dbUtil.getConnection();
            try (PreparedStatement statement = sectionConnection.prepareStatement(MASTER_TAB_DATA_INSERT)) {
                tabs.forEach(x -> {
                    try {
                        statement.setString(1, x);
                        statement.addBatch();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
                statement.executeBatch();
            }

            try (PreparedStatement sectionstatement = connection.prepareStatement(MASTER_TAB_SECTIONS_DATA_INSERT)) {
                tabs.forEach(x -> {
                    List<String> sections = getSectionNamesWithTabNameFromDB(x)
                            .stream()
                            .distinct()
                            .collect(Collectors.toList());
                    sections.forEach(y -> {
                        try {
                            sectionstatement.setString(1, x);
                            sectionstatement.setString(2, y);
                            sectionstatement.addBatch();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    });

                });
                sectionstatement.executeBatch();
            }

            try (PreparedStatement questionstatement = connection.prepareStatement(MASTER_TAB_SECTIONS_QUESTIONS_DATA_INSERT)) {
                tabs.forEach(x -> {

                    List<String> sections = getSectionNamesWithTabNameFromDB(x)
                            .stream()
                            .distinct()
                            .collect(Collectors.toList());


                    sections.forEach(y -> {
                        List<String> questions = getQuestionNamesWithSectionNameFromDB(y)
                                .stream()
                                .distinct()
                                .collect(Collectors.toList());

                        questions.forEach(z -> {
                            try {
                                questionstatement.setString(1, y);
                                questionstatement.setString(2, z);
                                questionstatement.addBatch();
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }

                        });

                    });

                });
                questionstatement.executeBatch();
            }
        } catch (Exception e) {
            Logger.getLogger("error {}",e.getMessage() );
        } finally {
            dbUtil.closeConnection(null);
        }
    }

    /*public static void main(String[] args){

        MasterAudit masterAudit = new MasterAudit();
        List<Tab> tabs = new ArrayList<>();
        Tab tab = new Tab();
        List<RowHeader> rowHeaders = new ArrayList<>();
        RowHeader rowHeader = new RowHeader();
        List <String> questionaire = new ArrayList<>();
        questionaire.add("How to make oil consumption");
        questionaire.add("How to reduce fuel prices");
        rowHeader.setQuestionnaire(questionaire);
        rowHeader.setHeaderName("Oil Consumption");
        rowHeaders.add(rowHeader);
        tab.setRowHeaders(rowHeaders);
        tab.setTabName("Oil Consumption");
        tabs.add(tab);

        Tab tab1 = new Tab();
        List<RowHeader> rowHeaderslist = new ArrayList<>();
        RowHeader rowHeader1 = new RowHeader();
        List <String> questionaire1 = new ArrayList<>();
        questionaire1.add("How to spend rupee consumption wisely");
        questionaire1.add("How to reduce rupee spenditure");
        rowHeader1.setQuestionnaire(questionaire1);
        rowHeader1.setHeaderName("Rupee spend");
        rowHeaderslist.add(rowHeader1);
        tab1.setRowHeaders(rowHeaderslist);
        tab1.setTabName("Rupee Usage");
        masterAudit.setTabs(tabs);
        tabs.add(tab1);
insertExcelMasterata(tabs);
    }*/

}
