package com.ibm.MiningApp.Dao;

import java.sql.*;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

public class DBUtil {

//        @Value("${url}")
        private String URL = "jdbc:mysql://localhost:3306/mining_app";
//        @Value("${username}")
        private String USERNAME = "test-user";
//        @Value("${password}")
        private String PASSWD = "test-password@1";
//        @Value("${classforname}")
        private String CLASSFOR = "com.mysql.cj.jdbc.Driver";


    private static final Logger logger = LoggerFactory.getLogger(DBUtil.class);

    private Connection connection = null;
    private Statement statement = null;
    private CallableStatement callStatement = null;
    PreparedStatement pstmt = null;


    public CallableStatement getCallableStatement(String query) {


        try {

            connection = getConnection();
            callStatement = connection.prepareCall(query);


        } catch (Exception e) {

            logger.error("Exception in executing preparedStatement : " + query + " : " + e);
        }
        return callStatement;
    }


    public ResultSet executePreparedStatment(String query) {

        ResultSet rs = null;

        try {

            connection = getConnection();
            pstmt = connection.prepareStatement(query);
            rs = pstmt.executeQuery();

        } catch (Exception e) {

            logger.error("Exception in executing preparedStatement : " + query + " : " + e);
        }
        return rs;
    }

    public int executeUpdate(String query) {

        int result = 0;

        try {
            connection = getConnection();
            statement = connection.createStatement();
            result = statement.executeUpdate(query);
        } catch (Exception e) {
            logger.error("Exception in executing statement : " + query + " : " + e);
        }

        return result;
    }

    public Connection getConnection() {

        try {

            System.out.println("Class :: " + CLASSFOR + "URL .... "+ URL + "USERName ... "+ USERNAME + " .. PassWd ..."+ PASSWD);
            Class.forName(CLASSFOR).newInstance();
            connection = DriverManager.getConnection(URL, USERNAME, PASSWD);

        } catch (Exception e) {

            logger.error("Exception in getting db connection : " + e);
        }

        return connection;
    }

    public void closeConnection(ResultSet rss) {

        try {

            if (rss != null && !rss.isClosed()) {

                rss.close();
            }
            if (statement != null && !statement.isClosed()) {

                statement.close();
            }
            if (pstmt != null && !pstmt.isClosed()) {

                pstmt.close();
            }
            if (callStatement != null && !callStatement.isClosed()) {

                callStatement.close();
            }
            if (connection != null && !connection.isClosed()) {

                connection.close();
            }

        } catch (Exception e) {

            logger.error("Error in closing the connection  : " + e);
        }
    }

    public Statement getStatement() {

        try {

            connection = getConnection();
            statement = connection.createStatement();

        } catch (Exception e) {

            logger.error("Exception in getStatement : " + e);
        }

        return statement;
    }

    public void executeBatch(PreparedStatement preparedStatement, Connection connection) {
        try {
            connection.setAutoCommit(false);
            preparedStatement.executeBatch();
            connection.commit();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                System.out.println("Transaction is being rolled back.");
                connection.rollback();
            } catch (Exception exe) {
                exe.printStackTrace();
            }
        }
    }
}
