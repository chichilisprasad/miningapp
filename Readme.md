**Pre-requisites**
Maven & Java 8 
**DB Configuration Changes:**
In DBUtil.java, update the url,username and password fields with the DB credentials
**Building:**
mvn clean install
**Execution:**
mvn spring-boot:run

**Data Base Schema:**

create table master_data1
(
id           int auto_increment,
tabName      varchar(100)  null,
sectionName  varchar(255)  null,
questionName varchar(2000) null,
constraint master_data_id_uindex
unique (id)
);

alter table master_data1
add primary key (id);


create table master_Auditing_Areas1
(
tabId   int auto_increment,
tabName varchar(255) null,
constraint master_tab_tabId_uindex
unique (tabId)
);

alter table master_Auditing_Areas1
add primary key (tabId);


create table master_Auditing_Area_Section1
(
sectionId   int auto_increment,
tabId       varchar(255)  null,
sectionName varchar(255)  null,
isDeleted   int default 0 null,
constraint master_tab_sections_sectionId_uindex
unique (sectionId)
);

alter table master_Auditing_Area_Section1
add primary key (sectionId);


create table master_Auditing_Area_Section_Questions1
(
questionId   int auto_increment,
sectionName  varchar(255)  null,
questionName varchar(1000) null,
isDeleted    int default 0 null,
constraint master_tab_sections_questions_questionId_uindex
unique (questionId)
);

alter table master_Auditing_Area_Section_Questions1
add primary key (questionId);

